from src import blueprints


def test_create_response_success_1():

    """ Should return a flask.Response with the given body """

    status = 200
    data = {'message': 'dummy message'}
    response = blueprints.create_response(data)
    assert response.status_code == status
    body = response.json
    assert 'message' in body
    assert body['message'] == data['message']


def test_create_response_success_2():

    """ Should return a flask.Response with the given body and status """

    status = 201
    data = {'message': 'dummy message'}
    response = blueprints.create_response(data, status)
    assert response.status_code == status
    body = response.json
    assert 'message' in body
    assert body['message'] == data['message']


def test_handle_errors_success():

    """ Should run a decorated method successfully """

    def dummy(message):
        return message

    message = 'dummy message'
    decorated = blueprints.handle_errors(dummy)
    result = decorated(message)
    assert result == message


def test_handle_errors_fail():

    """ Should generate a server error flask.Response when a decorated method raises an exception """

    def dummy(message):
        raise Exception(message)

    status = 500
    message = 'dummy message'
    decorated = blueprints.handle_errors(dummy)
    result = decorated(message)
    assert result.status_code == status
    body = result.json
    assert 'message' in body
    assert body['message'] == message
